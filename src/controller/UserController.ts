import { Request, Response, Router } from "express";
import { UserStore } from "../Store/StoreUser";
import { User } from "../Models/User";
import { validate } from "class-validator";

export const userRouter = Router();

export const userStore = new UserStore();

userRouter.get("/user", async (req:Request, res:Response) => {
  try {
    let user = await userStore.getAll();
    res.json(user);
  } catch (error) {
    res.status(500).json(error);
  }
});
userRouter.get("/user/:id", async (req, res) => {
  const user = await userStore.getByID(Number.parseInt(req.params.id))
  console.log(user);
  
  res.json(user);
});
userRouter.post("/user", async (req, res) => {
    const user = new User(req.body.name, req.body.email, req.body.password, req.body.type,req.body.salt);
    validate(user).then(errors =>{
      if (errors.length > 0) {
        res.status(400).json(errors)
      } else {
        userStore.add(user).then(data => res.status(201).json(data));
      }
    });
});
userRouter.patch('/user/:id', async (req, res) => {
  const user = await userStore.getByID(Number.parseInt(req.params.id));
  if (!user) {
    res.statusMessage = 'User not found!'
    res.status(404).end();
    return;
  }
  validate(user).then(errors =>{
    if (errors.length > 0) {
      res.status(400).json(errors)
    } else {
      console.log('validation succeed');
      userStore.update(user).then(data => res.status(202).json(data));  
      user.update(req.body)
    }
  });
});
userRouter.delete('/user/:id', async (req, res) => {
  const user = await userStore.getByID(Number.parseInt(req.params.id));
  if (!user) {
      res.status(404).end();
      return;
  }
  userStore.delete(user)
  res.statusMessage = "Deleted!"
  res.status(202).end()
  return;
})
