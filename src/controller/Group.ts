import { Request, Response, Router } from "express";
import { StoreGroup } from "../Store/StoreGroup";
import { Group } from "../Models/Group";
import { validate } from "class-validator";

export const groupRouter = Router();
export const storeGroup = new StoreGroup();

groupRouter.get("/group", async (req:Request, res:Response) => {
    try {
      let group = await storeGroup.getAll();
      res.json(group);
    } catch (error) {
      res.status(500).json(error);
    }
});

groupRouter.post("/group", async (req, res) => {
    const group = new Group(req.body.name, req.body.Date);
    validate(group).then(errors =>{
        if (errors.length > 0) {
            res.status(400).json(errors)
        } else {
            storeGroup.add(group).then(data => res.status(201).json(data));
        }
    });
});

groupRouter.get("/group/:id", async (req, res) => {
    const group = await storeGroup.getByID(Number.parseInt(req.params.id))
    res.json(group);
});

groupRouter.patch('/group/:id', async (req, res) => {
    const group = new Group(req.body.name, new Date(req.body.Date), Number.parseInt(req.params.id));
    validate(group).then(errors =>{
      if (errors.length > 0) {
        res.status(400).json(errors)
      } else {
        storeGroup.update(group).then(result => {
            if(result instanceof Error ){
                res.status(400).json(result);
            }
            res.status(202).json(result);
        });  
        group.update(req.body)
      }
    });
});

groupRouter.delete('/group/:id', async (req, res) => {
    storeGroup.delete(Number.parseInt(req.params.id)).then( result => {
        if(result instanceof Error ){
            res.status(400).json(result);
        }
        res.status(202).json(result);
    });
})
