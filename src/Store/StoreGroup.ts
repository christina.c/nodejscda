import { DAOGroup } from "../DAOs/DAOGroup";
import { Group } from "../Models/Group";

export class StoreGroup{

    private daoGroup:DAOGroup;

    constructor() {
        this.daoGroup = new DAOGroup();
    }

    async getAll() {
        let result = await this.daoGroup.getAll();
        return result;
    }

    async add(group:Group){
        
        let result = await this.daoGroup.add(group);
        if(typeof(result) === 'number')
        {
            return {'succes': true};
        }else{
            return {'error' : result};
        }
    }

    async getByID(id:number):Promise< Group | null >{
        let result = await this.daoGroup.getById(id);
        return result;
    }

    async delete(id:number){
        let group = await this.getByID(id);
        let result = await this.daoGroup.delete(group);
        return result;
    }

    async update(group:Group){
        let result = await this.daoGroup.update(group);
        return result;
    }
}
