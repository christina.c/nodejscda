import {
    Length,
    IsEmail,
   IsNotEmpty
  } from 'class-validator';
  
  export class User {
    id: number
    @Length(10, 20)
    @IsNotEmpty()
    name: string;
    @IsEmail()
    @IsNotEmpty()
    email: string;
    @Length(10)
    @IsNotEmpty()
    salt:string;
    @Length(8)
    @IsNotEmpty()
    password:string
    @IsNotEmpty()
    @Length(2)
    type:string
  
      constructor(
        name:string,
        email: string,
        password: string,
        type: string,      
        salt: string,
        id?: number,
        ){
  
          this.id=id,
          this.name=name,
          this.email=email,
          this.password=password,
          this.type=type,
          this.salt=salt
        }
      getID(): number{
        return this.id;
      }
      setID(id:number){
        this.id= id;
      }
      getName(): string{
        return this.name;
      }
      setName(name:string){
        this.name= name;
      }
      getEmail(): string{
        return this.email;
      }
  
      setEmail(email:string){
        this.email= email
      }
      
      getPassword():string {
          return this.password;
      }
      
      setPassword(password:string) {
          this.password = password;
      }
  
      getType():string {
        return this.type;
      }
      
      setType(type:string) {
          this.type = type;
      }
  
      getSalt():string {
          return this.salt;
      }
      
      setSalt(salt:string) {
        this.salt=salt;
      }
      update(data:User){
        Object.assign(this,data)
    }
  }
  