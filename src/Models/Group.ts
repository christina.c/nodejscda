import {
    IsDate,
    IsNotEmpty,
    IsString
  } from 'class-validator';

export class Group{

    @IsString()
    @IsNotEmpty()
    private name:string;
    @IsDate()
    @IsNotEmpty()
    private Date:Date;
    private id:number;

    constructor(name:string,Date:Date,id?:number){
        this.id = id;
        this.name = name;
        this.Date = Date;
    }
    
    /**
     * @return id:number
     */
    getId():number{
        return this.id;
    }

    /**
     * @return name:string
     */
    getName():string{
        return this.name;
    }


    /**
     * @return date:Date
     */
    getDate():Date{
        return this.Date;
    }

    /**
     * @param id 
     */
    setId(id:number){
        this.id = id;
    }

    /**
     * @param name 
     */
    setName(name:string){
        this.name = name;
    }
    
    /**
     * 
     * @param date 
     */
    setDate(date:Date){
        this.Date = date;
    }

    update(data:Group){
        Object.assign(this,data)
    }
}
