import { DAOGroup } from "../DAOs/DAOGroup";
import { Group } from "../Models/Group";

export class GroupBusiness{

    private repo:DAOGroup;

    constructor() {
        this.repo = new DAOGroup();
    }

    async getAll() {

        let recipes = await this.repo.getAll();
        return recipes;
    }

    async add(data:string[]) {
        let group = new Group(data['name'],data['date'],data['id']);
        let recipes = await this.repo.add(group);
        return recipes;
    }

}
