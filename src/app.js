import { userRouter } from "./src/controller/UserController";
import { groupRouter } from './src/controller/GroupController';
const bodyParser = require('body-parser');

export class App {

    constructor(){
        this.app = express();
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
    }
    async listen(){
        await this.app.listen(3000);
        console.log('Server on port',3000)
        
    }
    registerRouter(){
        this.app.use(userRouter)
        this.app.use(groupRouter)
    }

}

