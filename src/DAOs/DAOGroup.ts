import { connection } from "../connectDB/ConnectDB";
import { Group } from  "../Models/Group";
import { ResultSetHeader, RowDataPacket } from "mysql2";


export class DAOGroup{

    constructor(private db = connection) {}

    async getAll(): Promise<Group[]> {
            let [results] = await this.db.query<RowDataPacket[]>("SELECT * FROM Group");
            return results.map(row => new Group(row['id'], row['name'], row['Date']));
    }

    async add(group:Group){
        try{
            let [result] = await this.db.query<ResultSetHeader>('INSERT INTO Group (name,Date) VALUES (?,?)', [
                group.getName(),
                group.getDate(),
            ]);
            group.setId(result.insertId);
            return group;
        }catch(e){
            return e;
        }
    }

    async update(group:Group) {
        try{
            let [result] = await this.db.query<ResultSetHeader>('UPDATE Group SET name = ?, Date = ?', [
                group.getName(),
                group.getDate(),
            ]);
            return result;
        } catch(e){
            return e;
        }
    }

    async getById(id:number){
        let [results] = await this.db.query<RowDataPacket[]>("SELECT * FROM Group WHERE idName=?",[id]);
        if(results.length > 0) {
            return new Group(results['name'],results['Date']);
        }
        return null;
    }
    
    async delete(group:Group){
        try{
            let [result] = await this.db.query<ResultSetHeader>('DELETE FROM Group WHERE idGroup=?', [
                group.getId()
            ]);
            return result;
        } catch(e){
            return e;
        }
    }
}
