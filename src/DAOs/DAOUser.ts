import { validate } from "class-validator";
import { promisify } from "util";
import { connection } from "../connectDB/ConnectDB";
import { User } from "../Models/User";
import { ResultSetHeader, RowDataPacket } from "mysql2";

export class DAOUser {
  

  constructor(private db = connection) {}

  async getAll(): Promise<User[]> {
    let [result] = await this.db.query<RowDataPacket[]>("SELECT * FROM User");
    return result.map(
      (row) =>
        new User(
          row["name"],
          row["email"],
          row["password"],
          row["type"],
          row["salt"],
          row["idUser"]
        )
    );
  }

  async add(user: User): Promise<User> {
    let [result] = await this.db.query<ResultSetHeader>(
      "INSERT INTO User (name,email,password,type,salt) VALUES (?,?,?,?,?)",
      [
        user.name,
        user.email,
        user.password,
        user.type,
        user.salt,
      ]
    );
    user.setID(result.insertId);
    return user;
  }

  async getByID(id: number): Promise<User> {
    let [results] = await this.db.query<RowDataPacket[]>("SELECT * FROM User where idUser=?", [id]);
    if (results.length > 0) { 
      const user = new User(
        results[0]["name"],
        results[0]["email"],
        results[0]["password"],
        results[0]["type"],
        results[0]["salt"],
        results[0]["idUser"]
      );
      return user
    }
    return null;
  }

  async delete(user: User): Promise<ResultSetHeader>{
    const [result] = await this.db.query<ResultSetHeader>("DELETE FROM User WHERE idUser=?", [user.getID()])
    return result;
  }

  async update(user: User) {
    const result = await this.db.query<ResultSetHeader>(
      "UPDATE User SET name=?, email=?, password=?, type=?, salt=? WHERE idUser=?",
      [
        user.name,
        user.email,
        user.password,
        user.type,
        user.salt,
        user.id
      ]
    );
    return result;
  }
}
